<?php
require('top.inc.php');
isAdmin();
if(isset($_GET['type']) && $_GET['type']!=''){
	$type = $_GET['type'];
	if($type=='delete'){
		$id=$_GET['id'];
		$delete_sql="DELETE from newsfeed where Id=$id";
		mysqli_query($con,$delete_sql);
	}
}

$sql = $con->query("SELECT * from newsfeed ORDER by Id desc");
?>
<div class="content pb-0">
	<div class="orders">
	   <div class="row">
		  <div class="col-xl-12">
			 <div class="card">
				<div class="card-body">
				   <h4 class="box-title">POSTS</h4>
				</div>
				<div class="card-body--">
				   <div class="table-stats order-table ov-h">
					  <table class="table ">
						 <thead>
							<tr>
							   <th class="serial">#</th>
							   <th >ID</th>
							   <th >Post Author</th>
							   <th >Post Date</th>
							   <th >Post Body</th>
							   <th>Action</th>
							   </tr>
						 </thead>
						 <tbody>
							<?php 
							$i=1;
							while($row = $sql->fetch_array()){?>
							<tr>
							   <td class="serial"><?php echo $i?></td>
							   <td><?php echo $row['Id']?></td>
							   <td><?php echo $row['poster_name']?></td>
							   <td><?php echo $row['feed_date']?></td>
							   <td><?php echo $row['feed_content'] ;?> <br> <?php if(!empty($row['feed_img'])){ ?><img src="<?php echo $baseurl .'img/feedsimg/'. $row['feed_img'] ?>" width="100%"><?php }?><br><?php if(!empty($row['feed_video'])){ ?><video width='100%' controls><source src='<?php echo $baseurl."feedvideos/".$row['feed_video'] ?>' type='video/mp4'></video><?php } ?> </td>
							   <td><?php										
								echo "<span class='badge badge-delete'><a href='?type=delete&id=".$row['Id']."'>Delete</a></span>";
								?>
							   </td>
							</tr>
							<?php $i++; } ?>
						 </tbody>
					  </table>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
</div>
<?php
require('footer.inc.php');
?>