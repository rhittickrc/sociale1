<?php
require('top.inc.php');
isAdmin();
if(isset($_GET['type']) && $_GET['type']!=''){
	$type=$_GET['type'];
	if($type=='delete'){
		$id=$_GET['id'];
		$delete_sql="DELETE from products where id=$id";
		mysqli_query($con,$delete_sql);
	}
}

$sql = $con->query("SELECT * from products order by id desc");
?>
<div class="content pb-0">
	<div class="orders">
	   <div class="row">
		  <div class="col-xl-12">
			 <div class="card">
				<div class="card-body">
				   <h4 class="box-title">PRODUCT MANAGEMENT </h4>
				   <h4 class="box-link"><a href="../add-product.php">ADD Product</a> </h4>
				</div>
				<div class="card-body--">
				   <div class="table-stats order-table ov-h">
					  <table class="table ">
						 <thead>
							<tr>
							   <th class="serial">#</th>
							   <th>ID</th>
							   <th>Owner</th>
							   <th>Product Title</th>
							   <th>Product Image</th>
							   <th>Product Price</th>
							   <th>Product Discounted Price</th>
							   <th>Product Link</th>
							   <th>Product Description</th>
							   <th>Product Category</th>
							   <th>Action</th>
							</tr>
						 </thead>
						 <tbody>
							<?php 
							$i=1;
							while($row=$sql->fetch_array()){
								$idu = $row['idUsers'];
								$query = $con->query("SELECT f_name, l_name FROM users WHERE idUsers=$idu");
								$row1 = $query->fetch_array();
								?>
							<tr>
							   <td class="serial"><?php echo $i?></td>
							   <td><?php echo $row['id']?></td>
							   <td><?php echo $row1['f_name'].' '.$row1['l_name']?></td>
							   <td><?php echo $row['product_name']?></td>
							   <td><img src="<?php echo $baseurl."img/productimg/".$row['product_image'] ?>" style="width:100%;height:auto;"></td>
							   <td><?php echo $row['actual_price']?></td>
							   <td><?php echo $row['discounted_price']?></td>
							   <td><a href="<?php echo $row['product_link']?>" target="_blank"><?php echo $row['product_link']?></a></td>
							   <td><?php echo $row['product_description']?></td>
							   <td><?php echo $row['product_category']?></td>
							   <td>
								<?php
														
								echo "<span class='badge badge-delete'><a href='?type=delete&id=".$row['id']."'>Delete</a></span>";
								
								?>
							   </td>
							</tr>
							<?php $i++;} ?>
						 </tbody>
					  </table>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
</div>
<?php
require('footer.inc.php');
?>