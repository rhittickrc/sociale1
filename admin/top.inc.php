<?php
require('connection.inc.php');
require('functions.inc.php');
if($_SESSION['role'] == 'admin'){

}else{
   header('location:login.php');
   die();
} 
?>
<!doctype html>
<html class="no-js" lang="">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ADMIN DASHBOARD PAGE</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/normalize.css">
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets/css/themify-icons.css">
      <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
      <link rel="stylesheet" href="assets/css/flag-icon.min.css">
      <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
      <link rel="stylesheet" href="assets/css/style.css">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/js/all.min.js"></script>
   </head>
   <body>
      <aside id="left-panel" class="left-panel">
         <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
               <ul class="nav navbar-nav">
                  <li class="menu-title">MENU</li>
                  <?php if($_SESSION['role'] != " "){?>
				   <li class="menu-item-has-children dropdown">
                     <a href="product_management.php" > Product MANAGEMENT </a>
                  </li>
				  <li class="menu-item-has-children dropdown">
                     <a href="categories.php" > CATEGORIES </a>
                  </li>
				
                  
				  <li class="menu-item-has-children dropdown">
                     <a href="users.php" > USER MANAGEMENT </a>
                  </li>
               <li class="menu-item-has-children dropdown">
                  <a href="vendor.php" > Vendor MANAGEMENT </a>
               </li>
				  <li class="menu-item-has-children dropdown">
                     <a href="feeds_admin.php" > Feed Posts </a>
                  </li>
				  <li class="menu-item-has-children dropdown">
                     <a href="blog_admin.php" > Blog Posts</a>
                  </li>
				  <?php } ?>
               </ul>
            </div>
         </nav>
      </aside>
      <div id="right-panel" class="right-panel">
         <header id="header" class="header">
            <div class="top-left">
               <div class="navbar-header">
                  <a class="navbar-brand" href="/"><img src="https://socialemall-1.codtrees.com/img/cropped-Social-E-Mall-Copy%20copy.png" alt="Logo"></a>
                  <a class="navbar-brand hidden" href="index.php"><img src="https://socialemall-1.codtrees.com/img/cropped-Social-E-Mall-Copy%20copy.png" alt="Logo"></a>
                  <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
               </div>
            </div>
            <div class="top-right">
               <div class="header-menu">
                  <div class="user-area dropdown float-right">
                     <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">WELCOME <?php echo $_SESSION['f_name']?></a>
                     <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="logout.php"><i class="fa fa-power-off"></i>LOGOUT</a>
                     </div>
                  </div>
               </div>
            </div>
         </header>