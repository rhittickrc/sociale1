<?php
require('top.inc.php');
isAdmin();
if(isset($_GET['type']) && $_GET['type']!=''){
	$type=$_GET['type'];
	print_r($type);
	if($type=='delete'){
		$id=$_GET['id'];
        print_r($id);
		$delete_sql="DELETE FROM users WHERE idUsers=$id";
		$result = mysqli_query($con,$delete_sql);

	}
}

$query = $con->query("SELECT * FROM users WHERE role='vendor'");
?>
<div class="content pb-0">
	<div class="orders">
	   <div class="row">
		  <div class="col-xl-12">
			 <div class="card">
				<div class="card-body">
				   <h4 class="box-title">Users </h4>
				</div>
				<div class="card-body--">
				   <div class="table-stats order-table ov-h">
					  <table class="table ">
						 <thead>
							<tr>
							   <th class="serial">#</th>
							   <th>ID</th>
							   <th>Name</th>
							   <th>Email</th>
							   <th>Role</th>
						       <th>User Image</th>
							   <th></th>
							</tr>
						 </thead>
						 <tbody>
							<?php 
							$i=1;
							while($row = $query->fetch_array()){?>
							<tr>
							   <td class="serial"><?php echo $i?></td>
							   <td><?php echo $row['idUsers']?></td>
							   <td><?php echo $row['f_name'].' '. $row['l_name']?></td>
							   <td><?php echo $row['emailUsers']?></td>
							   <td><?php echo $row['role']?></td>
							   <td><img src=<?php echo $baseurl."uploads/".$row['userImg'];  ?>></td>
							   <td>
								<?php
								echo "<span class='badge badge-delete'><a href='?type=delete&id=".$row['idUsers']."'>Delete</a></span>";
								?>
							   </td>
							</tr>
							
							<?php $i++; } ?>
						 </tbody>
					  </table>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
</div>
<?php
require('footer.inc.php');
?>