<?php 
        require 'includes/dbh.inc.php'; 

$search_value=$_POST['search_sidebar_category'];
if($search_value=='all'){
	$query="SELECT * FROM products";

}
else{
$query="SELECT * FROM products WHERE product_category LIKE '%{$search_value}%'";
}
$result=mysqli_query($conn,$query) or die("Query Failed");
?>

<div class="row">

<?php

if(mysqli_num_rows($result) > 0){
	
	while($row=mysqli_fetch_assoc($result)){
		$product_id=$row['id'];
                $product_name=$row['product_name'];
                $product_description= $row['product_description'];
                $product_image= $row['product_image'];
                $product_category=$row['product_category'];
                $actual_price=$row['actual_price'];
                $discounted_price=$row['discounted_price'];
                $product_link=$row['product_link'];

?>

<div class="col-md-6 col-lg-6 col-sm-12 product-archive">
<div class="pro-cat">
<p><?php echo $product_category ?></p>
</div>
<div class="prodet">
             <div class="proimage">
<img src="<?php echo "img/productimg/".$product_image ?>" style="width:100%;height:auto;">
</div>
<div class="product-title">
             <h2><a class="underline" href="product.php/?id=<?php echo $product_id ?>&&name=<?php echo $product_name; ?>"><?php echo $product_name; ?></a></h2>
             </div>
             <hr/>
<div class="price row">
  <?php if( $discounted_price!=""){ ?>
           <div class="col-md-6"> 
<p class="actual-price strikethrough">₹ <?php echo $actual_price; ?></p>
</div>
          <div class="col-md-6">
<p class="discount">₹ <?php echo $discounted_price; ?></p>
</div>
<?php }else{?>

            <p class="actual-price">₹ <?php echo $product_price ?></p>
<?php
 } ?>


</div>

<div class="pro-link">
                          <p style="text-align:center;">  <a href="<?php echo $product_link ?>" target="_blank" class="btn btn-dark">Buy Now</a></p>
</div>
</div>
</div>

<?php
	}
}
?>

</div>
<?php
?>