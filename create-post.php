<?php
$title="Create Post";
    require 'includes/dbh.inc.php'; 
    include 'includes/header.php';
    ?>
   
<?php          
      if(isset($_SESSION['userId'])){

	$user_id=$_SESSION['userId'];


if(isset($_POST['submit'])){

	$post_title=mysqli_real_escape_string($conn,$_POST['post_title']);
	$post_content=$_POST['post-content'];
	    $filename = $_FILES["postimg"]["name"];
	        $tempname = $_FILES["postimg"]["tmp_name"];   
	                $folder = "img/postsimg/".$filename;


	$query="INSERT into posts(idUSers,post_title,post_content,post_img) VALUES('$user_id','$post_title',
'$post_content','$filename')";
	 $result= mysqli_query($conn, $query);

	              move_uploaded_file($tempname, $folder) ;

    
    if($result){
    	?>
    		<script type="text/javascript">
    			window.location.href = "blog";

    		</script>
    	<?php
    		//header('Location:/socialemall/codbucket/blog.php');
	    }
    else{
    echo "Error entering values:" . mysqli_error($conn);

    }



 
}

?>
<!-- Main Quill library -->
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>

<!-- Theme included stylesheets -->
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="https://cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">

<!-- Core build with no theme, formatting, non-essential modules -->
<link href="https://cdn.quilljs.com/1.3.6/quill.core.css" rel="stylesheet">
<script src="https://cdn.quilljs.com/1.3.6/quill.core.js"></script>


<div class="row">

 <div class="col-md-3 col-xs-12">   
        <?php include 'includes/left_sidebar.inc.php'; ?>
    </div>
    <div class="col-md-6 col-xs-12">
                <h1 style="text-align:center;">Add Blog</h1>

<form action="create-post.php" method="post" enctype="multipart/form-data">
	  <div class="form-group">
    <label for="post_title">Post Title</label>
    <input type="text" class="form-control" id="post_title" name="post_title" placeholder="Post Title" required>
  </div>
  <div id=toolbar></div>
  <div class="form-group" id="editor">
    <label for="post-content">Post Content</label>
    
    <textarea  class="form-control" id="editor" name="post-content" rows="3" placeholder="Post Content"></textarea>
  </div>
  <script>
 var options = {
  debug: 'info',
  modules: {
    toolbar: '#toolbar'
  },
  placeholder: 'Compose an epic...',
  readOnly: true,
  theme: 'snow'
};
var editor = new Quill('#editor', options);
</script>

    <div class="form-group">
    <label for="postimg">Post Thumbnail</label>
    <input type="file" class="form-control-file" id="postimg" name="postimg">
  </div>
	<input type="submit" name="submit" class="btn btn-primary" value="submit">


	<!--<input type="text" name="post_title" id="post_title" placeholder="Post Title">
	<textarea name="post-content" id="post-content" placeholder="Post Content"></textarea>
	<input type="file" name="postimg" id="postimg">
	<input type="submit" name="submit" value="submit">-->

</form>
</div>
 <div class="col-md-3 col-xs-12 col-12">
        <?php include 'includes/right_sidebar.inc.php'; ?>
    </div>
</div>

<?php }?>