<?php

    $title="Edit Profile";
    include 'includes/header.php';
    
    if (!isset($_SESSION['userId']))
    {
        header("Location: index.php");
        exit();
    }
    
?>
<div class="row">
    <div class="col-md-3 col-xs-12">   
        <?php include 'includes/left_sidebar.inc.php'; ?>
    </div>
    <div class="col-md-6 col-xs-12">
<div style="text-align: center">
    <img id="usercpe" src=<?php echo "./uploads/".$_SESSION['coverimg'];  ?> width="100%" height="auto">
    <img id="userDpe" src=<?php echo "./uploads/".$_SESSION['userImg'];  ?> width="200px" >
    <?php $fullname = $_SESSION['f_name'] .'&nbsp;'.$_SESSION['l_name'] ; ?>
    <h1><?php echo $fullname; ?></h1>
</div>


<?php
    
        $userName = '';
        $email = '';
    
        if(isset($_GET['error']))
        {
            if($_GET['error'] == 'emptyemail')
            {
                echo '<p class="closed">*Profile email cannot be empty</p>';
                $email = $_GET['mail'];
            }
            else if ($_GET['error'] == 'invalidmail')
            {
                echo '<p class="closed">*Please enter a valid email</p>';
            }
            else if ($_GET['error'] == 'emptyoldpwd')
            {
                echo '<p class="closed">*You must enter the current password to change it</p>';
            }
            else if ($_GET['error'] == 'emptynewpwd')
            {
                echo '<p class="closed">*Please enter the new password</p>';
            }
            else if ($_GET['error'] == 'emptyreppwd')
            {
                echo '<p class="closed">*Please confirm new password</p>';
            }
            else if ($_GET['error'] == 'wrongpwd')
            {
                echo '<p class="closed">*Current password is wrong</p>';
            }
            else if ($_GET['error'] == 'samepwd')
            {
                echo '<p class="closed">*New password cannot be same as old password</p>';
            }
            else if ($_GET['error'] == 'passwordcheck')
            {
                echo '<p class="closed">*Confirmation password is not the same as the new password</p>';
            }
        }
        else if (isset($_GET['edit']) == 'success')
        {
            echo '<p class="open">*Profile Updated Successfully</p>';
        }
?>


<form action="includes/profileUpdate.inc.php" method='post' id="contact-form" enctype="multipart/form-data">

        <h5>Personal Information</h5>
        <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" id="email" class="form-control" name="email" placeholder="email" 
               value=<?php echo $_SESSION['emailUsers']; ?>><br>
           </div>
       </div>
   </div>
        
        
             <div class="row">
                <div class="col-md-12">
                        <label>Full Name</label>
</div>
                <div class="col-md-6">
                                        <div class="form-group">

        <input type="text" id="f-name" class="form-control" name="f-name" placeholder="First Name" 
               value=<?php echo $_SESSION['f_name']; ?>>
           </div>
           </div>
                           <div class="col-md-6">
                                                <div class="form-group">

        <input type="text" id="l-name" class="form-control" name="l-name" placeholder="Last Name" 
               value=<?php echo $_SESSION['l_name']; ?>>
</div>
</div>
        </div>
        <div class="row">
<div class="col-md-12">
    <label for="gender">Gender</label>
                <div class="form-check">
          <input type="radio" class="form-check-input"  name="gender" value="m" id="gender-m"
                 <?php if ($_SESSION['gender'] == 'm'){ ?> 
                 checked="checked"
                 <?php } ?>>
                 <label class="form-check-label" for="gender-m">Male

                    <span class="checkmark"></span>
                    </label>
             </div>
                <div class="form-check">
      
          <input type="radio" class="form-check-input" name="gender" value="f" id="gender-f"
                 <?php if ($_SESSION['gender'] == 'f'){ ?> 
                 checked="checked"
                 <?php } ?>>
        <label class="form-check-label" for="gender-f">Female

                    <span class="checkmark"></span>
                    </label>
    </div>
</div>
</div>
<div class="row">
          <div class="col-md-12">
<div class="form-group">
        <label for="headline">Profile Headline</label>
        <input type="text" id="headline" name="headline" class="form-control" placeholder="Your Profile Headline"
               value='<?php echo $_SESSION['headline']; ?>'><br>

           </div>
       </div>
   </div>
     <div class="row">
          <div class="col-md-12">
<div class="form-group">   
        <label for="bio">Profile Bio</label>
        <textarea id="bio" name="bio" class="form-control" maxlength="5000"
            placeholder="What you want to tell people about yourself" 
            ><?php echo $_SESSION['bio']; ?></textarea>
          </div>
       </div>
   </div>
        <h5>Change Password</h5>
        <div class="row">
        <div class="col-md-12">
            <div class="form-group">

        <input type="password" id="old-pwd" class="form-control" name="old-pwd" placeholder="current password"><br>
</div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
        <input type="password" id="pwd" name="pwd" class="form-control" placeholder="new password">
    </div>
</div>
            <div class="col-md-6">
                <div class="form-group">

        <input type="password" id="pwd-repeat" class="form-control" name="pwd-repeat" placeholder="repeat new password">
        </div>
    </div>
    </div>
            <div class="form-group">
    <label for="dp">Profile Picture</label>

        <div class="upload-btn-wrapper">
            <button class="btn">Upload a file</button>
            <input type="file" id="dp" name='dp' class="form-control" value=<?php echo $_SESSION['userImg']; ?>>
        </div>
    </div>

              <div class="form-group">
    <label for="cp">Cover Picture</label>
        <div class="upload-btn-wrapper coverpic">
            <button class="btn">Upload a file</button>
            <input type="file" name='cp' class="form-control" id="cp" value=<?php echo $_SESSION['coverimg']; ?>>
        </div>
        </div>
        <input type="submit" class="button next btn-primary"  name="update-profile" value="Update Profile">
        
    </form>

<hr>
                    </div>
                    <div class="col-md-3 col-xs-12 col-12">
        <?php include 'includes/right_sidebar.inc.php'; ?>
    </div>
</div>
<?php include 'includes/footer.php'; ?> 

