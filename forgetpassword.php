<?php 
    define('TITLE',"Home | SocialEMall");
    include 'includes/header.php';
?>
   <body>
      <div class="container">
          <div class="card">
            <div class="card-header text-center">
              Please Enter Your Email Address
            </div>
            <div class="card-body">
              <form action="password-reset-token.php" method="post">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp">
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <input type="submit" name="password-reset-token" class="btn btn-primary">
              </form>
            </div>
          </div>
      </div>
 
   </body>
</html>

<?php 
    include 'includes/footer.php';
?>