 <?php
 require 'includes/dbh.inc.php';
 include ('function.php');
 session_start(); 
 if(isset($_SESSION['userId'])){
	if(isset($_POST["action"]))
	{
		 if($_POST["action"] == 'send_request'){
			// sleep(5);
			$data = array(
				':request_from_id'				=>	$_SESSION['userId'],
				':request_to_id'				=>	$_POST['to_id'],
				':request_status'				=>	'Pending',
				':request_notification_status'	=>	'No'
			);
			$request_from_id = $data[':request_from_id'];
			$request_to_id = $data[':request_to_id'];
			$request_status = $data[':request_status'];
			$request_notification_status = $data[':request_notification_status'];
			$query = "
			INSERT INTO friend_request 
			(request_from_id, request_to_id, request_status, request_notification_status) 
			VALUES ('$request_from_id', '$request_to_id', '$request_status', '$request_notification_status')
			";
			//echo '<pre>';print_r($query);die('noo');
			 $result= mysqli_query($conn, $query);
			 echo 'done';
			//$result->execute($data);
		 }
		 if($_POST["action"] == "count_un_seen_friend_request")
		{
			$query = "
			SELECT COUNT(request_id) as Total 
			FROM friend_request 
			WHERE request_to_id = '".$_SESSION['userId']."' 
			AND request_status = 'Pending' 
			AND request_notification_status = 'No'
			";

			$statement= mysqli_query($conn, $query);
			$result = mysqli_fetch_all($statement);
			foreach($result as $row)
			{
				echo $row[0];
			}
			
		}
		 
		if($_POST["action"] == "load_friend_request_list")
		{
			//sleep(5);
			$query = "
			SELECT * FROM friend_request 
			WHERE request_to_id = '".$_SESSION['userId']."' 
			AND request_status = 'Pending' 
			ORDER BY request_id DESC
			";

			$statement= mysqli_query($conn, $query);
			$result = mysqli_fetch_all($statement);
			//echo '<pre>';print_r($result);die();
			$output = '';

			foreach($result as $row)
			{
				$user_data = Get_user_profile_data($row[1], $conn);

				$user_name = '';

				foreach($user_data as $user_row)
				{
					$user_name = $user_row[1].' '.$user_row[2];
				}

				$output .= '
				
				<li>
					'.Get_user_avatar($row[1], $conn).'&nbsp;<b class="text-primary">'.$user_name . '</b>
					<button type="button" name="accept_friend_request_button" class="btn btn-primary btn-sm pull-right accept_friend_request_button" data-request_id="'.$row[0].'" id="accept_friend_request_button_'.$row[0].'"><i class="fa fa-plus" aria-hidden="true"></i> Accept</button>&nbsp;<button type="button" name="delete_friend_request_button" class="btn btn-primary btn-sm pull-right delete_friend_request_button" req_uid ="'.$row[0].'" data-request_id="delete_'.$row[0].'" id="delete_friend_request_button_'.$row[0].'"><i class="fa fa-minus" aria-hidden="true"></i>Decline</button>
				</li>
				
				
				';
			}
			echo $output;
		}
		if($_POST["action"] == 'remove_friend_request_number')
		{
			$query = "
			UPDATE friend_request 
			SET request_notification_status = 'Yes' 
			WHERE request_to_id = '".$_SESSION['userId']."' 
			AND request_notification_status = 'No'
			";
			$statement= mysqli_query($conn, $query);
			
		}
		if($_POST["action"] == 'accept_friend_request')
		{
			//sleep(5);
			$query = "
			UPDATE friend_request 
			SET request_status = 'Confirm' 
			WHERE request_id = '".$_POST["request_id"]."'
			";
			$statement= mysqli_query($conn, $query);
			
		}
		if($_POST["action"] == 'delete_friend_request')
		{
			//sleep(5);
			$query = "
			UPDATE friend_request 
			SET request_status = 'Reject' 
			WHERE request_id = '".$_POST["req_uid"]."'
			";
			//print_r($query);die('123');
			$statement= mysqli_query($conn, $query);
			
		}
		if($_POST["action"] == 'load_friends')
		{
			$query = "
			SELECT users.f_name, users.l_name, friend_request.request_from_id, friend_request.request_to_id FROM users 
			INNER JOIN friend_request 
			ON friend_request.request_from_id = users.idUsers 
			OR friend_request.request_to_id = users.idUsers 
			WHERE (friend_request.request_from_id = '".$_SESSION['userId']."' OR friend_request.request_to_id = '".$_SESSION['userId']."') 
			AND users.idUsers != '".$_SESSION['userId']."'
			AND friend_request.request_status = 'Confirm' 		
			GROUP BY users.f_name 
			ORDER BY friend_request.request_id DESC
			";	

			$statement= mysqli_query($conn, $query);
			$result = mysqli_fetch_all($statement);
			$row_count = count($result);
			//echo '<pre>';print_r($result);die('working');
			$html = '';

			if($row_count > 0)
			{
				
				$count = 0;

				foreach($result as $row)
				{
					$temp_user_id = 0;

					if($row[2] == $_SESSION['userId'])
					{
						$temp_user_id = $row[3];
					}
					else
					{
						$temp_user_id = $row[2];
					}

					$count++;

					if($count == 1)
					{
						$html .= '<div class="accepted_friends row">';
					}

					$html .= '
					<div class="col-md-4 col-xs-12">
					<div class="load_friend" style="margin-bottom:12px;">
						'.Get_user_avatar_big($temp_user_id, $conn).'
						<p><a href="profile?jncopabc='.$temp_user_id.'" style="font-size:12px;">'.Get_user_name($conn, $temp_user_id).'</a></p></div>
					</div>
					';

					if($count == 3)
					{
						$html .= '</div>';
						$count = 0;
					}
				}
			}
			else
			{
				$html = '<h4 align="center">No Friends Found</h4>';
			}
			echo $html;
		}
	}
	 
 }