	<?php 
		$title = "Profile";
		include('function.php');
		include('includes/header.php');
	?>
	<div class="row">
    <div class="col-md-3 col-xs-12">   
        <?php include 'includes/left_sidebar.inc.php'; ?>
    </div>

	<div class="col-md-6 col-xs-12">
     
	<div class="panel panel-default">
		<div class="panel-heading">
					<h3 class="panel-title">Friends</h3>
				<div class="panel-body">
					<div id = "friends-list"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-xs-12 col-12">
        <?php include 'includes/right_sidebar.inc.php'; ?>
    </div>
</div>
<?php 
	include ('includes/footer.php');
?>