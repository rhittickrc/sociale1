<?php
 
 require 'includes/dbh.inc.php';
 function wrap_tag($argument){
	 return '<b>'.$argument.'</b>';
 }
 
 function Get_user_avatar($user_id, $conn)
{
	$query = "
	SELECT userImg FROM users
    WHERE idUsers = '".$user_id."'
	";
	$statement=mysqli_query($conn,$query);
	$result = mysqli_fetch_all($statement);
	//echo'<pre>';print_r($result);die('okkkkkkkkkkkkkkkkkkk');
	foreach($result as $row)
	{
		return '<img src= "uploads/'.$row[0].'" width="25" class="img-circle" />';
	}
}
function Get_request_status($conn, $from_user_id, $to_user_id)
{
    $output = '';

    $query = "
    SELECT request_status 
    FROM friend_request 
    WHERE (request_from_id = '".$from_user_id."' AND request_to_id = '".$to_user_id."') 
    OR (request_from_id = '".$to_user_id."' AND request_to_id = '".$from_user_id."') 
    AND request_status != 'Confirm'
    ";
    $statement = mysqli_query($conn,$query);
	$result = mysqli_fetch_all($statement);
    foreach($result as $row)
    {
		$output = $row[0];
    }
	return $output;
}

function Get_user_profile_data($user_id, $conn)
{
    $query = "
    SELECT * FROM users 
    WHERE idUsers = '".$user_id."'
    ";
	$statement = mysqli_query($conn,$query);
	$result = mysqli_fetch_all($statement);
	return $result;
}

function Get_user_name($conn, $user_id)
{
    $query = "
    SELECT f_name, l_name
    FROM users 
    WHERE idUsers = '".$user_id."' 
    ";
    $statement = mysqli_query($conn,$query);
	$result = mysqli_fetch_all($statement);
    foreach($result as $row)
    {
		$f_name = $row[0];
		$l_name = $row[1];
		$full_name = $f_name.' '.$l_name;
        return $full_name;
    }
}
function Get_user_avatar_big($user_id, $conn)
{
    $query = "
    SELECT userImg FROM users 
    WHERE idUsers = '".$user_id."'
    ";
    $statement = mysqli_query($conn,$query);
	$result = mysqli_fetch_all($statement);
    foreach($result as $row)
    {
        return '<img src="uploads/'.$row[0].'" class="img-responsive img-circle" />';
    }
}
function get_user_id($user_id, $conn){
	
}
function fetch_user_chat_history($from_user_id, $to_user_id, $conn)
{
	$query = "
	 SELECT * FROM Chat 
	 WHERE (from_user_id = '".$from_user_id."' 
	 AND to_user_id = '".$to_user_id."') 
	 OR (from_user_id = '".$to_user_id."' 
	 AND to_user_id = '".$from_user_id."') 
	 ORDER BY timestamp DESC
	 ";
	$statement= mysqli_query($conn, $query);
	$result = mysqli_fetch_all($statement);
	//echo '<pre>';print_r($result);die('okkkkkk');
	 $output = '<ul class="list-unstyled">';
	 foreach($result as $row)
	 {
	  $user_name = '';
	  if($row[2] == $from_user_id)
	  {
	   $user_name = '<b class="text-success">You</b>';
	  }
	  else
	  {
	   $user_name = '<b class="text-danger">'.Get_user_name($conn, $row[2]).'</b>';
	  }
	  $output .= '
	  <li style="border-bottom:1px dotted #ccc">
	   <p>'.$user_name.' - '.$row[3].'
		<div align="right">
		 - <small><em>'.$row[4].'</em></small>
		</div>
	   </p>
	  </li>
	  ';
	 }
	 $output .= '</ul>';
	 $query = "
	 UPDATE Chat 
	 SET status = '0' 
	 WHERE from_user_id = '".$to_user_id."' 
	 AND to_user_id = '".$from_user_id."' 
	 AND status = '1'
	 ";
	 $statement= mysqli_query($conn, $query);
	
	 return $output;
}
function count_unseen_message($from_user_id, $to_user_id, $conn)
{
	$query = "
	SELECT * FROM Chat
	WHERE from_user_id = '$from_user_id' 
	AND to_user_id = '$to_user_id' 
	AND status = '1'
	";
	$statement= mysqli_query($conn, $query);
	$result = mysqli_fetch_all($statement);
	
	$count = count($result);
	$output = '';
	if($count > 0)
	{
		$output = '<span class="label label-success">'.$count.'</span>';
	}
	return $output;
}
