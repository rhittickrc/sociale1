<?php

    // nav items   
    $navItems = array(
                        array(
                            'slug' => "/",
                            'title' => "Home"
                        )
                    );
    
    $navItems_signedin = array(
                        array(
                            'slug' => "/",
                            'title' => "<i class='fa fa-home'></i> Home"
                        ),
                        array(
                            'slug' => "edit-profile",
                            'title' => "<i class='fa fa-address-card-o'></i> My Profile"
                        ),
                        array(
                            'slug' => "blog",
                            'title' => "<i class='fa fa-list'></i> BLog"
                        ),
                        array(
                            'slug' => "photos",
                            'title' => "<i class='fa fa-image'></i> Gallery</i> "
                        ),
						array(
                            'slug' => "friends",
                            'title' => "<i class='fa fa-user'></i> Friends</i> "
                        ),
                            array(
                            'slug' => "create-post",
                            'title' => "<i class='fa fa-feed'></i> Add Blog</i> "
                        ),
                    );