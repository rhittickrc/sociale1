<div class="feeds">
<?php
include('like_backend.php');
if (isset($_SESSION['userId']))
{

    $logged_user_id=$_SESSION['userId'];

    $query = "SELECT Id,idUSers,poster_name,feed_content,feed_img,feed_video,feed_date FROM newsfeed ORDER BY feed_date DESC";
    $result = mysqli_query($conn, $query);
    if (mysqli_num_rows($result) > 0)
    {
        while ($row = mysqli_fetch_array($result))
        {
            $feed_id = $row['Id'];
            //$feed_id_new[]=$feed_id;
            $user_id = $row['idUSers'];
            $query2 = "SELECT userImg from users where idUsers=$user_id";
            $result2 = mysqli_query($conn, $query2);
            $result_new = mysqli_fetch_all($result2);
            $user_img = $result_new[0][0];
            $poster_name = $row['poster_name'];
            $feed_content = $row['feed_content'];
            $feed_img = $row['feed_img'];
            //print_r($feed_img);
            //echo "<pre>"; print_r($feed_img);
            $image_feed = '';
            $video_feed = '';

            $feed_date = $row['feed_date'];
            $feed_video=$row['feed_video'];

?>
            <div class="row post">
                <div class="col-md-1 col-sm-1 col-lg-1 padzero">
        <img class="userimagepost" src="<?php echo "uploads/" . $user_img ?>" style="width:100px;height: auto;border:1px solid #000;">
      </div>
                      <div class="col-md-10 col-sm-10 col-lg-10">

                      <p><span class="username"><?php echo $poster_name; ?></span><br>
                      <span class="datepost"><?php echo $feed_date; ?></span></p>
                        <hr>
</div>
<?php if($logged_user_id==$user_id){ ?>
 <div class="col-md-1 col-sm-1 col-lg-1">
     <div class="dropdown">
  <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-pencil" aria-hidden="true"></i>

  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="feed-edit?edit=<?php echo $feed_id ?>">Edit</a>
    <a class="dropdown-item delete" href="delete-confirm?delete=<?php echo $feed_id ?>">Delete</a>
  </div>
</div>

 </div>
<?php } ?>   
    <div class="col-lg-12 col-sm-12 col-md-12 padzero">  
    <?php if(!empty($feed_img)){ ?> 
                    <div class="row">  
                  <?php
                  $feed_img1 = explode(",", $feed_img);
                  //echo"<pre>";print_r($feed_img1);die();
                  $remove_Arr = array_shift($feed_img1);
                  if (count($feed_img1) > 1) {
                  //echo"<pre>";print_r($feed_img1);die();
                    foreach($feed_img1 as $feed_img_show){

              $value_image = pathinfo($feed_img_show, PATHINFO_EXTENSION);


              if($value_image == "jpg" || $value_image == "JPG" || $value_image == "png" || $value_image == "PNG" || $value_image == "jpeg" || $value_image == "JPEG" || $value_image == "gif"){
                $image_feed = $feed_img_show;
                //echo"<pre>";print_r($image_feed);die();
              }
              
              ?>
              
              
              <div class="col-md-6 fimg"><?php
              if(!empty($image_feed)){
                ?>
                                    <div class="gallerys"> 

                <?php
              echo "<a href='img/feedsimg/".$image_feed."' target='_blank'><img src='img/feedsimg/".$image_feed."' class='feedimg' style='width:100%;height:auto;'/></a>";
                }?>
              </div>
                <?php
              ?>
              </div> <?php
                }
            }else{
                foreach($feed_img1 as $feed_img_show){

                  $value_image = pathinfo($feed_img_show, PATHINFO_EXTENSION);
    
    
                  if($value_image == "jpg" || $value_image == "JPG" || $value_image == "png" || $value_image == "PNG" || $value_image == "jpeg" || $value_image == "JPEG" || $value_image == "gif"){
                    $image_feed = $feed_img_show;
                    //echo"<pre>";print_r($image_feed);die();
                  }
                  
                  ?>
                  
                  
                  <div class="col-md-12"><?php
                  if(!empty($image_feed)){
                  echo "<a href='img/feedsimg/' ".$image_feed."' target='_blank'><img src='img/feedsimg/".$image_feed."' class='feedimg' style='width:100%;height:auto;'/></a>";
                    }
                  ?>
                  </div> <?php
                }
              }
             ?>
            </div>
    <!-- <a href="<?php //echo "img/feedsimg/" . $feed_img ?>" target="_blank"><img src="<?php// echo "img/feedsimg/" . $feed_img ?>" class="feedimg" style="width:100%;height:auto;" /></a>-->
<!--   </div>
 -->    <?php } ?>
    <?php if(!empty($feed_video)){ ?>  
      <div class="row">  
      <?php
                  $feed_video1 = explode(",", $feed_video);
                  $remove_Arr = array_shift($feed_video1);
                  if (count($feed_video1) > 1) {
                    foreach($feed_video1 as $feed_video_show){
              $value_video = pathinfo($feed_video_show, PATHINFO_EXTENSION);
              if($value_video == "mp4" || $value_video == "m4v" || $value_video == "mov" || $value_video == "wmv"){
                $video_feed = $feed_video_show;
              }
              ?>
              <div class="col-md-6"><?php
              if(!empty($video_feed)){
              echo "<video width='100%' controls><source src='feedvideos/".$video_feed." ' type='video/mp4'> </video>";
              }
              ?>
              </div> <?php
            }
           }else{
            foreach($feed_video1 as $feed_video_show){
              $value_video = pathinfo($feed_video_show, PATHINFO_EXTENSION);
              if($value_video == "mp4" || $value_video == "m4v" || $value_video == "mov" || $value_video == "wmv"){
                $video_feed = $feed_video_show;
              }
              ?>
              <div class="col-md-12"><?php
              if(!empty($video_feed)){
              echo "<video width='100%' controls><source src='feedvideos/".$video_feed." ' type='video/mp4'> </video>";
              }
              ?>
              </div> <?php
            }
          } ?>  
          </div>
<!--<video width='100%' controls><source src='<?php echo "feedvideos/".$feed_video ?>' type='video/mp4'>   </video>  -->         <?php } ?>
    <p class="feed-content"><?php echo $feed_content; ?></p>
    <div class="post_info">
      <div class="like-dislike">
          <!-- if user likes post, style button differently -->
          
            <i 
            <?php if(userLiked($feed_id)): ?>
              class ="fa fa-thumbs-up like-btn"
            <?php else: ?>
              class ="fa fa-thumbs-o-up like-btn"
            <?php endif ?>
            data-id="<?php echo $feed_id;?>"></i>
            <span class="likes"><?php echo getLikes($feed_id); ?></span>
            &nbsp;&nbsp;&nbsp;&nbsp;
            
            
            <!-- if user dislikes post, style button differently -->
            
            <i 
            <?php if (userDisliked($feed_id)): ?>
              class="fa fa-thumbs-down dislike-btn"
            <?php else: ?>
              class="fa fa-thumbs-o-down dislike-btn"
            <?php endif ?>
              data-id="<?php echo $feed_id;?>"></i>
            <span class="dislikes"><?php echo getDislikes($feed_id); ?></span>
        </div>
        <div class="share">
            <?php include ("includes/sharefeed.inc.php"); ?>
        </div>
    </div>
  </div>

<div class="col-lg-12 commentsarea padzero">
<div class="form-group">
    <textarea class="form-control" id="comments_new_<?php echo $feed_id; ?>" name="comments" placeholder="Comment"></textarea>
  </div>
          <?php
            $commenter_user_id = $_SESSION['userId'];
            $comment_user_fname = $_SESSION['f_name'];
            $comment_user_lastname = $_SESSION['l_name'];
            $commenter_user_name = $comment_user_fname . ' ' . $comment_user_lastname;
?>
<button id="comment_submit" class ="comment_submit btn btn-dark" commenter_user_id = "<?php echo $commenter_user_id; ?>" commenter_user_name="<?php echo $commenter_user_name; ?>" post_owner_user_id="<?php echo $user_id; ?> "  commented_post_id="<?php echo $feed_id; ?>" >Comment</button>
</div>
<!-- <div id="loader"></div>
 -->
<div class="comments">


  <?php

  $comments_query_result = "SELECT * FROM comments WHERE commented_post_id=$feed_id";
      $result_comment_query = mysqli_query($conn, $comments_query_result);
            $result_comments = mysqli_fetch_all($result_comment_query);
           
            foreach($result_comments as $comment){
              $userid = $comment[1];
              $usert = "SELECT * FROM users WHERE idUsers=$userid";
              $rusert = mysqli_query($conn, $usert);
              $ruserf = mysqli_fetch_all($rusert);
              foreach($ruserf as $cname){
              $fname = $cname[1] .'&nbsp;'. $cname[2];
        $iu = $cname[9];
        $udp = "<img src='uploads/$iu' width='30px'>";
              }
              $fetched_comments='<br>'.$udp.'&nbsp;<b>&nbsp;&nbsp;'.$fname .' :</b>&nbsp;&nbsp;'. $comment[5];
              echo $fetched_comments.'<br>';
            } ?>
            <!-- <a href=""  data-toggle="modal" data-target="#exampleModal">See more...</a> -->
            </div>
            </div>
            <?php
} 
} 
}?>
</div>
<script>
  $(document).ready(function(){
    $('.gallerys').magnificPopup({
      type:'image',
      delegate:'a',
      gallery:{
        enabled:true
      }
    }); 
  });
</script>