<div class="sidetogglemobile">
  <button type="button" class="sidetoggle btn btn-dark" style="margin-bottom:10px;">    <i class="fa fa-bars"></i></button>
</div>
<div class="leftside">
    <!-------     LOGIN / LOGOUT FORM               --------->
    <div id="login">         
          <?php 
              
              if(isset($_SESSION['userId']))
              { 
                   /* if($_SESSION['role'] == 'admin'){
                        echo '<p>Welcome Admin</p>
                        <a href="admin.php" class="adash">Admin Dashboard</a>';
                    } */
                    
                    $imgurl1 = $baseurl.'/uploads/'.$_SESSION["coverimg"];
                    $imgurl2 = $baseurl.'/uploads/'.$_SESSION["userImg"];
                  echo'<div style="text-align: center;">
                        <div class="coverdp" width="100%">
                        <img id="usercp" src='.$imgurl1.' width="100%">
                          <img id="userDp" src='.$imgurl2.' width="100px">
                          </div>
                          <h3>' . $_SESSION['userUid'] . '</h3>
                      </div>';
              }
              else
              {
                  if(isset($_GET['error']))
                  {
                      if($_GET['error'] == 'emptyfields')
                      {
                          echo '<p class="closed">*please fill in all the fields</p>';
                      }
                      else if($_GET['error'] == 'nouser')
                      {
                          echo '<p class="closed">*username does not exist</p>';
                      }
                      else if ($_GET['error'] == 'wrongpwd')
                      {
                          echo '<p class="closed">*wrong password</p>';
                      }
                      else if ($_GET['error'] == 'sqlerror')
                      {
                          echo '<p class="closed">*website error. contact admint to have it fixed</p>';
                      }
                  }
  
                  echo '
                  <div class="loginform">
                  <form method="post" action="includes/login.inc.php" id="login-form">
                          <div class="form-group">
                      <input type="text" id="name" class="form-control" name="mailuid" placeholder="Username..." required>
                      
                      </div>
                      <div class="form-group">
                      <input type="password" id="password"class="form-control" name="pwd" placeholder="Password..." required>
                      </div>
                      <input type="submit" class="button next login btn btn-dark" name="login-submit" value="Login">
                  </form>
                  <hr/>
                  <div class="row">
                  <div class="col-md-4">
                  <a href="signup" class="button previous">Signup</a>
                  </div>
                                    <div class="col-md-8">
                  <a href="forgetpassword" class="forgetps">Forget Password</a>
                  </div>
                  </div>
                  </div>
                  ';
                  
              }
          ?> 
      </div>
    <!-------     LOGIN / LOGOUT FORM END           --------->
   <?php if(isset($_SESSION['userId'])){ ?>
    <div class="timeline">
    <?php include 'sidebar_menu.inc.php'; ?>
    </div>
    <?php }?>
    <div class="title">
      <br>
    <h3>Members</h3>
  </div>
  <?php include 'members.inc.php'; ?>


</div>

<script>
  $(document).ready(function(){
    $('.sidetoggle').click(function(){
      $('.leftside').toggle(1000);
    });
  });
</script>