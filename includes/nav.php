<style>
    .form-inline {
    display: inline-block;
}
.navbar-header.col {
    padding: 0 !important;
}   
.navbar {       
    background: #eee !important;
    padding-left: 16px;
    padding-right: 16px;
    border-bottom: 1px solid #d6d6d6;
    box-shadow: 0 0 4px rgba(0,0,0,.1);
}
.nav-link {
    margin: 0 5px;
}
.nav-link img {
    border-radius: 50%;
    width: 36px;
    height: 36px;
    margin: -8px 0;
    float: left;
    margin-right: 10px;
}
.navbar .navbar-brand {
    color: #555;
    padding-left: 0;
    font-size: 20px;
    padding-right: 50px;
    font-family: 'Raleway', sans-serif;
}
.navbar .navbar-brand b {
    font-weight: bold;
    color: #eb5844;
}
.navbar .navbar-nav a:hover, .navbar .navbar-nav a:focus {
    color: #f08373 !important;
}
.navbar .navbar-nav a.active, .navbar .navbar-nav a.active:hover {
    color: #eb5844 !important;
    background: transparent !important;
}
.search-box {
    position: relative;
}   
.search-box input.form-control {        
    padding-right: 35px;
    border-radius: 0;
    padding-left: 0;
    border-width: 0 0 1px 0;
    box-shadow: none;
}
.search-box input.form-control:focus {      
    border-color: #f08373;      
}
.search-box .input-group-text {
    min-width: 35px;
    border: none;
    background: transparent;
    position: absolute;
    right: 0;
    z-index: 9;
    padding: 7px 0 7px 7px;
    height: 100%;
}
.search-box i {
    color: #a0a5b1;
    font-size: 19px;
}
.navbar .nav-item i {
    font-size: 18px;
}
.navbar .dropdown-item i {
    font-size: 16px;
    min-width: 22px;
}
.navbar .nav-item.show > a {
    background: none !important;
}
.navbar .dropdown-menu {
    border-radius: 1px;
    border-color: #e5e5e5;
    box-shadow: 0 2px 8px rgba(0,0,0,.05);
}
.navbar .dropdown-menu a {
    color: #777;
    padding: 8px 20px;
    line-height: normal;
    font-size: 15px;
}
.navbar .navbar-form {
    margin-right: 0;
    margin-left: 0;
    border: 0;
}

.stickynav {
    position:fixed !important;
    background:#eee !important;
    z-index:99999 !important;
    width:100% !important;
        box-shadow: 0 2px 2px -2px rgba(0,0,0,0.4);
            transition: 0.2s;


}
@media (min-width: 992px){
    .form-inline .input-group {
        width: 250px;
        margin-right: 30px;
    }
}
@media (max-width: 991px){
    .form-inline {
        display: block;
        margin-bottom: 10px;
        margin-top: 0;
    }
    .input-group {
        width: 100%;
    }
}
</style>
 <?php 
	//session_start();

 ?>

<nav class="navbar navbar-expand-lg navbar-light navnormal">
    <a class="navbar-brand" href="/"><img src="<?php echo $baseurl ?>img/cropped-Social-E-Mall-Copy%20copy.png" class="img-fluid"></a>    
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
           <div class="navbar-form-wrapper" style="margin:0 auto !important;">
                <div class="input-group search-box">        
                    <form class="navbar-form navbar-right">
                        <div class = "input-group">
                            <input type="text" class="form-control" id="searchbar" placeholder="Search" autocomplete="off" />
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                        <div id="countryList" style="position:absolute; width:235px; z-index:1001;"></div>
                    </form>
                        
                </div>
            </div>
        <div class="navbar-nav ml-auto">
         
            <div class="navbar-nav">
            <a href="/" class="nav-item nav-link">Home</a>
            <a href="/blog" class="nav-item nav-link">Blog</a>         
            <a href="/shop" class="nav-item nav-link">Shop</a>
           
        </div>
            <?php if(isset($_SESSION['userId'])){ ?>
            <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="friend_request_area">
					<span id="unseen_friend_request_area"></span>
					<i class="fa fa-user-plus fa-2" aria-hidden="true"></i>
					<span class="caret"></span>
				</a>
                
				<ul class="dropdown-menu frndmenu" id="friend_request_list" style="width: 300px; max-height: 350px; margin-left: -50px;">

				</ul>
			</li>
			
            <a href="message?pabc=<?php echo $_SESSION['userId']; ?>" class="nav-item nav-link"><i class="fa fa-envelope"></i>	
				<span id="msg_notification"></span>
			</a>
			
            <a href="#" class="nav-item nav-link"><i class="fa fa-bell"></i>
				
			</a>
            <?php 
                $uid= $_SESSION['userId'];
                // echo $uid;
                 $query_top="SELECT * FROM users WHERE idUsers='$uid'";
                                  // print_r($query_top);

                 $result_top=mysqli_query($conn,$query_top);
                 $row_top= mysqli_fetch_array($result_top);
                 // print_r($row_top);
                 $ufname = $row_top['f_name']; 
                 $uimag = $row_top['userImg'];   



                 $urlid = $_SESSION['userId'];
             ?>
            <a href="profile?jncopabc=<?php echo $urlid; ?>" class="nav-item nav-link"><img id="puserDptop" src=<?php echo $baseurl."uploads/".$uimag; ?>><span class="udr">Hi! <?php echo $ufname; ?></span>
</a>
            <a href="/logout" class="nav-item nav-link"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
            <?php }?>
        </div>      
    </div>
</nav>


<script>
     $(window).scroll(function() {
   if ($(this).scrollTop() > 250){
      $('.navnormal').addClass("stickynav");
   } else {
      $('.navnormal').removeClass("stickynav");
   }
});
</script>