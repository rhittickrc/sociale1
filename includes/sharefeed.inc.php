<?php 
 
  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
       $url = "https://";   
  else  
       $url = "http://";   
  // Append the host(domain name, ip) to the URL.   
  $url.= $_SERVER['HTTP_HOST'];   
  
  // Append the requested resource location to the URL   
  $url.= $_SERVER['REQUEST_URI']; 

?>
              <i class="fa fa-share-alt" aria-hidden="true"></i> <a target="_blank" href="http://twitter.com/share?text=Visit the link &url=<?php echo $url; ?>"><i class="fa fa-twitter"></i></a>
                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $url; ?>"> <i class="fa fa-facebook"></i></a>
                <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>"><i class="fa fa-linkedin"></i></a>
