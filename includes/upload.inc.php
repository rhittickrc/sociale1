<?php

$file = $_FILES['dp'];
$filec = $_FILES['cp'];

if (!empty($_FILES['dp']['name']))
{
    $fileName = $_FILES['dp']['name'];
    $fileTmpName = $_FILES['dp']['tmp_name'];
    $fileSize = $_FILES['dp']['size'];
    $fileError = $_FILES['dp']['error'];
    $fileType = $_FILES['dp']['type']; 

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('jpg', 'jpeg', 'png', 'gif');
    if (in_array($fileActualExt, $allowed))
    {
        if ($fileError === 0)
        {
            if ($fileSize < 10000000)
            {
                $FileNameNew = uniqid('', true) . "." . $fileActualExt;
                $fileDestination = '../uploads/' . $FileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);

            }
            else
            {
                header("Location: ../signup.php?error=imgsizeexceeded");
                exit(); 
            }
        }
        else
        {
            header("Location: ../signup.php?error=imguploaderror");
            exit();
        }
    }
    else
    {
        header("Location: ../signup.php?error=invalidimagetype");
        exit();
    }
}

if (!empty($_FILES['cp']['name']))
{
    $filecName = $_FILES['cp']['name'];
    $filecTmpName = $_FILES['cp']['tmp_name'];
    $filecSize = $_FILES['cp']['size'];
    $filecError = $_FILES['cp']['error'];
    $filecType = $_FILES['cp']['type']; 

    $filecExt = explode('.', $filecName);
    $filecActualExt = strtolower(end($filecExt));

    $allowed = array('jpg', 'jpeg', 'png', 'gif');
    if (in_array($filecActualExt, $allowed))
    {
        if ($filecError === 0)
        {
            if ($filecSize < 10000000)
            {
                $FilecNameNew = uniqid('', true) . "." . $filecActualExt;
                $filecDestination = '../uploads/' . $FilecNameNew;
                move_uploaded_file($filecTmpName, $filecDestination);

            }
            else
            {
                header("Location: ../signup.php?error=imgsizeexceeded");
                exit(); 
            }
        }
        else
        {
            header("Location: ../signup.php?error=imguploaderror");
            exit();
        }
    }
    else
    {
        header("Location: ../signup.php?error=invalidimagetype");
        exit();
    }
}
