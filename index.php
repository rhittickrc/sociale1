<?php 
    define('TITLE',"Home | SocialEMall");
    $title="Home";

    include 'includes/header.php';
?>
<div class="row">
    <div class="col-md-3 col-xs-12">   
        <?php include 'includes/left_sidebar.inc.php'; ?>
    </div>
     
    <div class="col-md-6 col-xs-12">
        <div class="post-area">
            <?php if(!isset($_SESSION['userId'])) { 
                 include 'includes/join-us.php';
                 include 'includes/blogindex.inc.php';
            }?>  
            <?php include 'includes/feeds.php' ?>   
                        <?php include 'includes/feeds_display.php' ?>   

        </div>
    </div>
    <div class="col-md-3 col-xs-12 col-12">
        <?php include 'includes/right_sidebar.inc.php'; ?>
    </div>
</div>
<?php 
    include 'includes/footer.php';
?>
