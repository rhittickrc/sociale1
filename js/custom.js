jQuery(document).ready(function(){
	jQuery("textarea#post_feed").emojioneArea({
   	pickerPosition:"bottom"});
	unseen_message();
	setInterval(function(){
		update_chat_history_data();
		unseen_message();
		load_friends();
	}, 5000);
	jQuery('#searchbar').keyup(function(){
		var query = jQuery(this).val();
		if(query != ''){
			jQuery.ajax({
				url: "search_action.php",
				method: "POST",
				data: {query:query},
				success: function(data)
				{
					jQuery('#countryList').html(data);
				}
			});
		}else{
			jQuery('#countryList').html('');
		}
		return false;
	});
jQuery(document).on('click', '#comment_submit', function(){
	
	var commenter_user_id = jQuery(this).attr('commenter_user_id');
	var commenter_user_name = jQuery(this).attr('commenter_user_name');
	var post_owner_user_id = jQuery(this).attr('post_owner_user_id');
	var commented_post_id = jQuery(this).attr('commented_post_id');
	var comments=jQuery('#comments_new_'+commented_post_id).val();
	var comment_data = 'commenter_user_id='+commenter_user_id+'&commenter_user_name='+commenter_user_name+'&post_owner_user_id='+post_owner_user_id+'&commented_post_id='+commented_post_id+'&comments='+comments;
	jQuery.ajax({
		url:"comments.php",
		type:"post",	
	    data:comment_data,
		beforeSend:function(){
			
		},
		success:function(responseObj){
			jQuery("#comment_area").html(responseObj);
			location.reload();

		}
	});
		
	return false;
	});
	jQuery('.request_button').on('click',function(){
	var to_id = jQuery(this).attr('data_userid');
	var action = 'send_request';
	if(to_id > 0)
		{	
			jQuery.ajax({
				url:"friend_action.php",
				method:"POST",
				data:{to_id:to_id, action:action},
				beforeSend:function()
				{
					jQuery('#request_button_'+to_id).attr('disabled', 'disabled');
					jQuery('#request_button_'+to_id).html('<i class="fa fa-circle-o-notch fa-spin"></i> Sending...');
				},
				success:function(data)
				{
					jQuery('#request_button_'+to_id).html('<i class="fa fa-clock-o" aria-hidden="true"></i> Request Send');
				}
			});
		}
	});
	
	function count_un_seen_friend_request()
	{
		var action = 'count_un_seen_friend_request';

		jQuery.ajax({
			url:"friend_action.php",
			method:"POST",
			data:{action:action},
			success:function(data)
			{
				if(data > 0)
				{
					jQuery('#unseen_friend_request_area').html('<span class="label label-danger">'+data+'</span>');
					//is_open = 'no';
				}
			}
		})
	}

	setInterval(function(){
		 count_un_seen_friend_request();
	}, 5000);
	function load_friends_request_list_data()
	{
		var action = 'load_friend_request_list';
		jQuery.ajax({
			url:"friend_action.php",
			method:"POST",
			data:{action:action},
			beforeSend:function()
			{
				jQuery('#friend_request_list').html('<li align="center"><i class="fa fa-circle-o-notch fa-spin"></i></li>');
			},
			success:function(data)
			{
				jQuery('#friend_request_list').html(data);
				remove_friend_request_number();
			}
		})
	}	
	
	jQuery('#friend_request_area').click(function(){
		load_friends_request_list_data();
	});
	
	function remove_friend_request_number()
    {
    	jQuery.ajax({
    		url:"friend_action.php",
    		method:"POST",
    		data:{action:'remove_friend_request_number'},
    		success:function(data)
    		{
    			jQuery('#unseen_friend_request_area').html('');
    		}
    	})
    }
	jQuery('.frndmenu').click(function(event){

    	event.preventDefault();
		var req_uid = event.target.getAttribute('req_uid');
    	var request_id = event.target.getAttribute('data-request_id');
    	if(request_id > 0)
    	{
    		jQuery.ajax({
    			url:"friend_action.php",
    			method:"POST",
    			data:{request_id:request_id, action:'accept_friend_request'},
    			beforeSend:function()
    			{
    				jQuery('#accept_friend_request_button_'+request_id).attr('disabled', 'disabled');
    				jQuery('#accept_friend_request_button_'+request_id).html('<i class="fa fa-circle-o-notch fa-spin"></i> wait...');
    			},
    			success:function()
    			{
    				load_friends_request_list_data();
    			}
    		})
    	}
		if(request_id = "delete_"+req_uid){
			 jQuery.ajax({
    			url:"friend_action.php",
    			method:"POST",
    			data:{req_uid:req_uid, action:'delete_friend_request'},
    			beforeSend:function()
    			{
    				jQuery('#delete_friend_request_button_'+request_id).attr('disabled', 'disabled');
    				jQuery('#delete_friend_request_button_'+request_id).html('<i class="fa fa-circle-o-notch fa-spin"></i> wait...');
    			},
    			success:function()
    			{
    				load_friends_request_list_data();
    			}
    		}) 
		}
    	return false;
    });
	function make_chat_dialog_box(to_user_id, to_user_name)
	{
		var modal_content = '<div id="user_dialog_'+to_user_id+'" class="user_dialog" title="You have chat with '+to_user_name+'">';
		modal_content += '<div style="height:400px; background:white; border:1px solid #ccc; overflow-y: scroll; margin-bottom:24px; padding:16px;" class="chat_history" data-touserid="'+to_user_id+'" id="chat_history_'+to_user_id+'">';
		modal_content += fetch_user_chat_history(to_user_id);
		modal_content += '</div>';
		modal_content += '<div class="form-group">';
		modal_content += '<textarea name="chat_message_'+to_user_id+'" id="chat_message_'+to_user_id+'" class="form-control"></textarea>';
		modal_content += '</div><div class="form-group" align="right">';
		modal_content+= '<button type="button" name="send_chat" id="'+to_user_id+'" class="btn btn-info send_chat">Send</button></div></div>';
		jQuery('#user_model_details').html(modal_content);
	}
	jQuery('.chat_button').on('click',function(){
		var to_user_id = jQuery(this).attr('to_user_id');
		var to_user_name = jQuery(this).attr('to_user_name');
		//alert(to_user_name);
		make_chat_dialog_box(to_user_id, to_user_name);
		jQuery("#user_dialog_"+to_user_id).dialog({
		    autoOpen:false,
			width: 400,
			});
		jQuery('#user_dialog_'+to_user_id).dialog('open');
	});
	jQuery(document).on('click', '.send_chat', function(){
		var to_user_id = $(this).attr('id');
		var chat_message = jQuery('#chat_message_'+to_user_id).val();
		jQuery.ajax({
		url:"insert_chat.php",
		method:"POST",
		data:{to_user_id:to_user_id, chat_message:chat_message},
		success:function(data)
		{
			jQuery('#chat_message_'+to_user_id).val('');
			jQuery('#chat_history_'+to_user_id).html(data);
		}
	   });
    });
	function fetch_user_chat_history(to_user_id)
	{
		jQuery.ajax({
		url:"fetch_user_chat_history.php",
		method:"POST",
		data:{to_user_id:to_user_id},
		success:function(data){
		jQuery('#chat_history_'+to_user_id).html(data);
	   }
	  })
	}
	function update_chat_history_data()
	{
		jQuery('.chat_history').each(function(){
		var to_user_id = jQuery(this).data('touserid');
		fetch_user_chat_history(to_user_id);
		});
	}
	function unseen_message()
	{
		var action = "count_un_seen_msg"
		jQuery.ajax({
			url:"unseen_message.php",
			method:"POST",
			data:{action:action},
			success:function(data)
			{
				if(data > 0)
				{
					jQuery('#msg_notification').html('<span class="label label-danger">'+data+'</span>');
					//is_open = 'no';
				}
			}
		})
	}
	function load_friends()
    {
    	jQuery.ajax({
    		url:"friend_action.php",
    		method:"POST",
    		data:{action:'load_friends'},
    		success:function(data)
    		{
    			jQuery('#friends-list').html(data);
    		}
    	});
    }
	load_friends();
	
// if the user clicks on the like button ...	
jQuery('.like-btn').click(function(){
	var post_id = jQuery(this).data('id');	
	$clicked_btn = jQuery(this);
	if($clicked_btn.hasClass('fa-thumbs-o-up')){
		action_id = 'like';
	}else if($clicked_btn.hasClass('fa-thumbs-up')){
		action_id = 'unlike';
	}
	jQuery.ajax({
		url:'like_backend.php',
		type:'POST',
		data : {
			'action_id':action_id,
			'post_id':post_id
		},
		success:function(data){
			res = JSON.parse(data);
			if(action_id == 'like'){
				$clicked_btn.removeClass('fa-thumbs-o-up');
				$clicked_btn.addClass('fa-thumbs-up');
			}else if(action_id == 'unlike'){
				$clicked_btn.removeClass('fa-thumbs-up');
				$clicked_btn.addClass('fa-thumbs-o-up');
			}
			
			// display the number of likes and dislikes
			$clicked_btn.siblings('span.likes').text(res.likes);
			$clicked_btn.siblings('span.dislikes').text(res.dislikes);
			
			// change button styling of the other button if user is reacting the second time to post
			$clicked_btn.siblings('i.fa-thumbs-down').removeClass('fa-thumbs-down').addClass('fa-thumbs-o-down');
		}
	});
});

// if the user clicks on the dislike button ...

jQuery('.dislike-btn').click(function(){
	var post_id = jQuery(this).data('id');	
	$clicked_btn = jQuery(this);
	if($clicked_btn.hasClass('fa-thumbs-o-down')){
		action_id = 'dislike';
	}else if($clicked_btn.hasClass('fa-thumbs-down')){
		action_id = 'undislike';
	}
	jQuery.ajax({
		url:'like_backend.php',
		type:'POST',
		data : {
			'action_id':action_id,
			'post_id':post_id
		},
		success:function(data){
			res = JSON.parse(data);
			if(action_id == 'dislike'){
				$clicked_btn.removeClass('fa-thumbs-o-down');
				$clicked_btn.addClass('fa-thumbs-down');
			}else if(action_id == 'undislike'){
				$clicked_btn.removeClass('fa-thumbs-down');
				$clicked_btn.addClass('fa-thumbs-o-down');
			}
			
			// display the number of likes and dislikes
			$clicked_btn.siblings('span.likes').text(res.likes);
			$clicked_btn.siblings('span.dislikes').text(res.dislikes);
			
			// change button styling of the other button if user is reacting the second time to post
			$clicked_btn.siblings('i.fa-thumbs-up').removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');

		}
	});
});

jQuery(document).on('click', '.list-group-item', function(){
		jQuery('#searchbar').val(jQuery.trim($(this).text()));
		//$('#countryList').fadeOut();
		jQuery('#countryList').html('');

	});


/*product searcha ajax sidebar*/


jQuery('#search_product').on('keyup',function(){
	var search_term=jQuery(this).val();
			if(search_term != ''){

		jQuery.ajax({
			url:'includes/product-search.php',
			type:'POST', 
			data : {search_sidebar:search_term},
			success:function(data){
				jQuery('#searched_product').html(data);
			}
		});
	}
	else{
						jQuery('#searched_product').html('');

	}
	});
});
jQuery(document).ready(function(){
    jQuery("#share").click(function(){
        jQuery(".prolike").toggle();
    })
});

function openCity(evt, cityName) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
	  tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
	  tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(cityName).style.display = "block";
	evt.currentTarget.className += " active";
  }