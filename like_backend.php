<?php

require 'includes/dbh.inc.php';

// session_start();
	
	if(isset($_POST['action_id'])){
	$post_id = $_POST['post_id'];
	$action = $_POST['action_id'];
	//print_r($action);die();
	$logged_user = $_SESSION['userId'];
	switch ($action) {
  	case 'like':
         $sql="INSERT INTO rating_info (user_id, post_id, rating_action) 
         	   VALUES ('".$logged_user."', '".$post_id."', '".$action."') 
         	   ON DUPLICATE KEY UPDATE rating_action='like'";
         break;
  	case 'dislike':
          $sql="INSERT INTO rating_info (user_id, post_id, rating_action) 
               VALUES ('".$logged_user."', '".$post_id."', '".$action."') 
         	   ON DUPLICATE KEY UPDATE rating_action='dislike'";
         break;
  	case 'unlike':
	      $sql="DELETE FROM rating_info WHERE user_id='".$logged_user."' AND post_id='".$post_id."'";
	      break;
  	case 'undislike':
      	  $sql="DELETE FROM rating_info WHERE user_id='".$logged_user."' AND post_id='".$post_id."'";
      break;
  	default:
  		break;
  }
   mysqli_query($conn, $sql);
  echo getRating($post_id);
  exit(0);
}
function getRating($id)
{
  global $conn;
  $rating = array();
  $likes_query = "SELECT COUNT(*) FROM rating_info WHERE post_id = '".$id."' AND rating_action='like'";
  $dislikes_query = "SELECT COUNT(*) FROM rating_info 
		  			WHERE post_id = '".$id."' AND rating_action='dislike'";
  			
  $likes_rs = mysqli_query($conn, $likes_query);
  $dislikes_rs = mysqli_query($conn, $dislikes_query);
  
  $likes = mysqli_fetch_all($likes_rs);
  $dislikes = mysqli_fetch_all($dislikes_rs);
  $likes_count = count($likes);
  //print_r($likes_count);die('okkkkkkkk');
  $rating = [
  	'likes' => $likes[0],
  	'dislikes' => $dislikes[0]
  ];
  return json_encode($rating);
}
function getLikes($id)
{
  global $conn;
  $sql = "SELECT COUNT(*) FROM rating_info 
  		  WHERE post_id = '".$id."' AND rating_action='like'";
		  
  $rs = mysqli_query($conn, $sql);
  $result = mysqli_fetch_all($rs);
  //echo '<pre>';print_r($result);die('ok');
  return $result[0][0];
  
}

// Get total number of dislikes for a particular post
function getDislikes($id)
{
  global $conn;
  $sql = "SELECT COUNT(*) FROM rating_info 
  		  WHERE post_id = '".$id."' AND rating_action='dislike'";
		  
  $rs = mysqli_query($conn, $sql);
  
  $result = mysqli_fetch_all($rs);
  return $result[0][0];
}
function userLiked($post_id)
{
  global $conn;
  //global $user_id;
  
  $sql = "SELECT * FROM rating_info WHERE user_id='".$_SESSION['userId']."' 
  		  AND post_id='".$post_id."' AND rating_action='like'";
	//echo '<pre>';print_r($sql);	  die();
  $statement = mysqli_query($conn, $sql);
  $result = mysqli_fetch_all($statement);
  
  if ((count($result)) > 0) {
  	return true;
  }else{
  	return false;
  }
}

// Check if user already dislikes post or not
function userDisliked($post_id)
{
  global $conn;
  //global $user_id;
  $sql = "SELECT * FROM rating_info WHERE user_id='".$_SESSION['userId']."' 
  		  AND post_id='".$post_id."' AND rating_action='dislike'";
  $statement = mysqli_query($conn, $sql);
  $result = mysqli_fetch_all($statement);
  if ((count($result)) > 0) {
  	return true;
  }else{
  	return false;
  }
}

