<?php 
    define('TITLE',"Home | SocialEMall");
    $title="Home";

    include 'includes/header.php';
?>
<div class="row">
    <div class="col-md-3 col-xs-12">   
        <?php include 'includes/left_sidebar.inc.php'; ?>
    </div>
     
    <div class="col-md-6 col-xs-12">
<h1>Feed Images</h1>
<div class="row" id="gall">
<?php   
    echo ("<br>");
        if(isset($_SESSION['userId']))
        {
            $id_g=$_SESSION['userId'];
            $query = $conn->query("SELECT feed_img FROM newsfeed Where idUsers='$id_g'");
            while($row = $query->fetch_array()){
                $user_imgs = $row['feed_img'];
                if($user_imgs != ""){
                ?>
                <div class="col-md-4 photo Gallery">
                <span><img id="usercp" src=<?php echo "img/feedsimg/".$user_imgs; ?> width="100%" ><span>
                &nbsp;
                </div>
        <?php   }   
            }
        } ?>
</div>
<h1>Blog Images</h1>
<div class="row" id="gall">
<?php   if(isset($_SESSION['userId']))
        {
            $id_g1=$_SESSION['userId'];

            $query1 = $conn->query("SELECT post_img FROM posts Where idUsers='$id_g1'");
            while($row1 = $query1->fetch_array()){
                $user1_imgs = $row1['post_img'];
                if($user1_imgs != ""){
                ?>
                <div class="col-md-4 photo Gallery">
                <span><img id="usercp" src=<?php echo "img/postsimg/".$user1_imgs ; ?> width="100%" ><span>
                &nbsp;
                </div>
        <?php   }   
            }
        } ?>
</div>
<h1>Profile Images</h1>
<div class="row" id="gall">
<?php   if(isset($_SESSION['userId']))
        {
            $id_g2=$_SESSION['userId'];

            $query2 = $conn->query("SELECT * FROM users Where idUsers='$id_g'");
            while($row2 = $query2->fetch_array()){
                $user2_imgs = $row2['userImg'];
                $user3_cp = $row2['coverimg'];
                if($user2_imgs != ""){
                ?>
                <div class="col-md-4 photo Gallery">
                <span><img id="usercp" src=<?php echo "uploads/".$user2_imgs; ?> width="100%" ><span>
                &nbsp;
                </div>
                <?php   }
                if($user2_imgs != ""){
                    ?>
                    <div class="col-md-4 photo Gallery">
                    <span><img id="usercp" src=<?php echo "uploads/".$user3_cp; ?> width="100%" ><span>
                    &nbsp;
                    </div>
                <?php   }     
            }
        } ?>
</div>
</div>
<div class="col-md-3 col-xs-12 col-12">
        <?php include 'includes/right_sidebar.inc.php'; ?>
    </div>
</div>