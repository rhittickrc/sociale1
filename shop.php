<style type="text/css">
  .ajax-loader {
display: none;  
background-color: rgba(255,255,255,0.7);
  position: absolute;
  z-index: +100 !important;
  width: 100%;
  height:100%;
}

.ajax-loader img {
  position: relative;
  top:50%;
  left:30%;
}

</style>

<?php 
    $title="Shop";
    require 'includes/dbh.inc.php'; 
    include 'includes/header.php';
    ?>

<div class="ajax-loader">
  <img src="img/200.gif">
</div>
<div class="row">   
  <div class="col-md-3 col-lg-3 col-sm-12">
        <?php include 'includes/left_sidebar.inc.php'; ?>
  </div>
  <div class="col-md-6 col-lg-6 col-sm-12">
    

 <div class="post-area">
       <h1> Our Products </h1>

      


          <div class="product-search-category">
             <div class="dropdown">
<!--  <label for="product_category">Category</label>
 -->
<select name="search_product_category" id="search_product_category" class="form-control" required>
     <option value="all" default>All</option>

     <?php $query_cat="SELECT * FROM categories";

             $result_cat= mysqli_query($conn, $query_cat);
                        if(mysqli_num_rows($result_cat) > 0){
                                              while($row = mysqli_fetch_array($result_cat)){
                                                    $category_name=$row['category_name'];

   ?>
    <option value="<?php echo $category_name; ?>"><?php echo $category_name; ?></option>


<?php }}?>
</select>
</div>
 

<div id='searched_product_category'>

  
</div>      </div> 

<div class="row">
       <?php 
     // if(isset($_SESSION['userId'])){

           require 'includes/dbh.inc.php'; 


        $query="SELECT * FROM products ";
           $result= mysqli_query($conn, $query);

           if(mysqli_num_rows($result) > 0){
                      while($row = mysqli_fetch_array($result)){

                        //$user_id= $row['idUSers'];
                        $product_id=$row['id'];
                        $product_name=$row['product_name'];
                        $product_description= $row['product_description'];
                        $product_image= $row['product_image'];
                        $product_category=$row['product_category'];
                        $actual_price=$row['actual_price'];
                        $discounted_price=$row['discounted_price'];
                        $product_link=$row['product_link'];



?>
<div class="col-md-6 col-lg-6 col-sm-12 product-archive mainpro">
<div class="pro-cat">
<p><?php echo $product_category ?></p>
</div>
<div class="prodet">
             <div class="proimage">
<img src="<?php echo "img/productimg/".$product_image ?>" style="width:100%;height:auto;">
</div>
<div class="product-title">
             <h2><a class="underline" href="product.php/?id=<?php echo $product_id ?>&&name=<?php echo $product_name; ?>"><?php echo $product_name; ?></a></h2>
             </div>
             <hr/>
<div class="price row">
  <?php if( $discounted_price!=""){ ?>
           <div class="col-md-6"> 
<p class="actual-price strikethrough">₹ <?php echo $actual_price; ?></p>
</div>
          <div class="col-md-6">
<p class="discount">₹ <?php echo $discounted_price; ?></p>
</div>
<?php }else{?>

            <p class="actual-price">₹ <?php echo $product_price ?></p>
<?php
 } ?>


</div>

<div class="pro-link">
                          <p style="text-align:center;">  <a href="<?php echo $product_link ?>" target="_blank" class="btn btn-dark">Buy Now</a></p>
</div>
</div>
</div>
<?php

}
?>


             

<?php
}


          // }



       ?>
     </div>
</div>
  </div>
  <div class="col-md-3 col-lg-3 col-sm-12">
        <?php include 'includes/right_sidebar.inc.php'; ?>
  </div>
</div>

<script>
  $(document).ready(function(){

    jQuery('#search_product_category').change(function(){
  var search_term_category=jQuery(this).val();
    jQuery.ajax({
        beforeSend: function(){
    $('.ajax-loader').show();
  },
      url:'category-search',
      type:'POST', 
      data : {search_sidebar_category:search_term_category},
      success:function(data){
            $('.ajax-loader').hide();
        jQuery('#searched_product_category').html(data);
      }
    });
    $('.mainpro').css('display','none');

  });


  });

</script>