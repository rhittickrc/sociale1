<?php 
    if(isset($_SESSION['userId']))
    {
        header("Location: index.php");
        exit();
    }
    $title="Sing Up";
    include 'includes/header.php';
?>
<div class="row">
<div class="col-md-3 col-xs-12"> 
<?php include 'includes/left_sidebar.inc.php'; ?>  
</div>
<div class="col-md-6 col-xs-12">
        <div id="contact">
    
            <h1>Signup</h1>
                <?php
                
                    $userName = '';
                    $email = '';
                
                    if(isset($_GET['error']))
                    {
                        if($_GET['error'] == 'emptyfields')
                        {
                            echo '<p class="closed">*Fill In All The Fields</p>';
                            $userName = $_GET['uid'];
                            $email = $_GET['mail'];
                        }
                        else if ($_GET['error'] == 'invalidmailuid')
                        {
                            echo '<p class="closed">*Please enter a valid email and user name</p>';
                        }
                        else if ($_GET['error'] == 'invalidmail')
                        {
                            echo '<p class="closed">*Please enter a valid email</p>';
                        }
                        else if ($_GET['error'] == 'invaliduid')
                        {
                            echo '<p class="closed">*Please enter a valid user name</p>';
                        }
                        else if ($_GET['error'] == 'passwordcheck')
                        {
                            echo '<p class="closed">*Passwords donot match</p>';
                        }
                        else if ($_GET['error'] == 'usertaken')
                        {
                            echo '<p class="closed">*This User name is already taken</p>';
                        }
                        else if ($_GET['error'] == 'invalidimagetype')
                        {
                            echo '<p class="closed">*Invalid image type. Profile image must be a .jpg or .png file</p>';
                        }
                        else if ($_GET['error'] == 'imguploaderror')
                        {
                            echo '<p class="closed">*Image upload error</p>';
                        }
                        else if ($_GET['error'] == 'imgsizeexceeded')
                        {
                            echo '<p class="closed">*Image too large</p>';
                        }
                        else if ($_GET['error'] == 'sqlerror')
                        {
                            echo '<p class="closed">*Website Error: Contact admin to have the issue fixed</p>';
                        }
                    }
                    else if (isset($_GET['signup']) == 'success')
                    {
                        echo '<p class="open">*Signup Successful. Please login from the Login menu on the right</p>';
                    }
                ?>
                <form action="includes/signup.inc.php" method='post' id="contact-form" enctype="multipart/form-data">
<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <input type="text" id="name1" class="form-control" name="uid" placeholder="Username" value=<?php echo $userName; ?>>
                    <div id="usexs"></div>
                </div>
                                    <div class="form-group">
                    <input type="email" id="email" name="mail" class="form-control" placeholder="email" value=<?php echo $email; ?>>
                </div>
            </div>
        </div>
                <div class="row">
                <div class="col-md-6">
                                        <div class="form-group">
                    <input type="password" id="pwd" class="form-control" name="pwd" placeholder="password">
                </div>
            </div>
                                <div class="col-md-6">

                                    <div class="form-group">

                    <input type="password" id="pwd-repeat" class="form-control" name="pwd-repeat" placeholder="repeat password">
                </div>

</div>
</div>    
<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="dp">Profile Image</label>

                    <div class="upload-btn-wrapper">
                        <input type="file" name='dp' class="dp" id="dp">
                    </div>
                    <!-- <img id="userDp" src="" >-->
                    </div>

                </div>
                                
                    </div>
<div class="row">
<div class="col-md-12">
                <label for="gender">Gender</label>
                <div class="form-check">
                    <input type="radio" checked="checked" class="form-check-input" name="gender" value="m" id="gender-m">
                    <label class="form-check-label" for="gender-m">Male

                    <span class="checkmark"></span>
                    </label>
                </div>
                    <div class="form-check">
                     <input type="radio" name="gender"  class="form-check-input" value="f" id="gender-f">

                    <label class="form-check-label" for="gender-f">Female
                    <span class="checkmark"></span>
                    </label>
                </div>
                </div>
</div>
                    <h3>Optional</h3>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" id="f-name" name="f-name" class="form-control" placeholder="First Name" >
                         </div> 
                         </div> 
                    <div class="col-md-6">
                        <div class="form-group">
      
                            <input type="text" id="l-name" name="l-name"  class="form-control" placeholder="Last Name" >
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="form-group">

                             <input type="date" id="dob" name="dob" class="form-control" placeholder="Date of birth" >
                </div>
            </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <input type="text" id="headline" name="headline" class="form-control" placeholder="Your Profile Headline">
                    </div>
            </div>
                            <div class="col-md-12">
                    <div class="form-group">

                    <textarea id="bio" name="bio" class="form-control" placeholder="What you want to tell people about yourself"></textarea>
                </div>
                    </div>
                    <div class="col-md-12">

                    <input type="submit" class="button next btn btn-dark" name="signup-submit" value="Sign Up">
                </div>
                    </div>
                </form>
        </div>
</div>

    <div class="col-md-3 col-lg-3 col-sm-12">
        <?php include 'includes/right_sidebar.inc.php'; ?>
  </div>
</div>
<script>
    $(document).on('change','#name1',function(){
        console.log("changed");
        var item_id = $("#name1").val();
        $("#usexs").html(" ");
        //console.log(item_id);

        jQuery.ajax({
               url:'includes/username-check.php',
               method:'post',
               data:"code="+item_id,
               success:function(data){
                   $("#usexs").append(data);
               }
        });
    });
</script>
<?php include 'includes/footer.php'; ?> 
